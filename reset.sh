#!/bin/bash

systemctl restart bind
systemctl restart postfix
systemctl restart dovecot
systemctl restart spamassassin
systemctl restart apache2
service snapd restart
snap disable nextcloud
snap enable nextcloud