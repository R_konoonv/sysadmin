#!/bin/bash

#
# Execution example: ./bindconf.sh 192.168.1.90 arturerm exm erm-vm
#

if [ -z "$1" ] ; then
    echo 'External IP is not specified !!!'
    exit 1
fi

if [ -z "$2" ] ; then
    echo 'domain name is not specified !!!'
    exit 1
fi

if [ -z "$3" ] ; then
    echo 'tld is not specified !!!'
    exit 1
fi

if [ -z "$4" ] ; then
    echo 'hostname is not specified !!!'
    exit 1
fi

echo "IP="$1"; domain="$2"; tld="$3"; hostname="$4

#
# Backup
#
echo "Backup files to /etc/bak/ ..."
mkdir -p /etc/bak
mkdir -p /etc/bak/bind
mkdir -p /etc/bak/bind/zones
cp /etc/bind/*.* /etc/bak/bind/
cp /etc/bind/zones/*.* /etc/bak/bind/zones/
cp /etc/resolv.conf /etc/bak/resolv.conf
cp /etc/hostname /etc/bak/hostname
cp /etc/mailname /etc/bak/mailname
cp /etc/hosts /etc/bak/hosts

#
# named.conf.options
#

echo "Removing zone files..."
rm /etc/bind/zones/*.*

echo "Replaceing /etc/bind/named.conf.options ..."
cat <<EOF >/etc/bind/named.conf.options
acl goodclients{
    127.0.0.0/8;
};
options {
	directory "/var/cache/bind";

	// If there is a firewall between you and nameservers you want
	// to talk to, you may need to fix the firewall to allow multiple
	// ports to talk.  See http://www.kb.cert.org/vuls/id/800113

	// If your ISP provided one or more IP addresses for stable 
	// nameservers, you probably want to use them as forwarders.  
	// Uncomment the following block, and insert the addresses replacing 
	// the all-0's placeholder.

	// forwarders {
	// 	0.0.0.0;
	// };

	//========================================================================
	// If BIND logs error messages about the root key being expired,
	// you will need to update your keys.  See https://www.isc.org/bind-keys
	//========================================================================
	dnssec-enable yes;
	dnssec-validation yes;
	

	auth-nxdomain no;    # conform to RFC1035
	listen-on { 127.0.0.1; $1; };
	listen-on-v6 { none; };

};
EOF

#
# named.conf.local
#
echo "Replaceing /etc/bind/named.conf.local ..."
cat <<EOF >/etc/bind/named.conf.local
//
// Do any local configuration here
//

// Consider adding the 1918 zones here, if they are not used in your
// organization

view local_resolver {
        recursion yes;
        allow-query { goodclients; };
	match-clients { goodclients; };
	match-destinations { goodclients; };
	
	include "/etc/bind/named.conf.default-zones";
	include "/etc/bind/zones.rfc1918";

	zone "$2.$3" {
		type master;
		file "/etc/bind/zones/$2.$3.zone";
	};

	zone "$3" {
		type master;
		file "/etc/bind/zones/$3.zone";	
	
	};
};

view world_resolver {
	allow-query { any; };
	recursion yes;
        match-clients { any; };
        match-destinations { any; };	
        zone $2.$3 {
                type master;
                file "/etc/bind/zones/$2.$3.zone";
        };
};
EOF

#
# named.conf.logging
#
echo "Replaceing /etc/bind/named.conf.logging ..."
cat <<EOF >/etc/bind/named.conf.logging
logging {
        channel update_debug {
                file "/var/log/bind9/update_debug.log" versions 3 size 100k;
                severity debug;
                print-severity  yes;
                print-time      yes;
        };
        channel security_info {
                file "/var/log/bind9/security_info.log" versions 1 size 100k;
                severity info;
                print-severity  yes;
                print-time      yes;
        };
        channel bind_log {
                file "/var/log/bind9/bind.log" versions 3 size 1m;
                severity info;
                print-category  yes;
                print-severity  yes;
                print-time      yes;
        };

        category default { bind_log; };
        category lame-servers { null; };
        category update { update_debug; };
        category update-security { update_debug; };
        category security { security_info; };
};
EOF

#
# zones/domain.tld.zone
#
echo "Replaceing /etc/bind/zones/domain.tld.zone ..."
cat <<EOF >/etc/bind/zones/$2"."$3".zone"
;
; BIND data file for local zone $2.$3;
\$TTL    15M
@       IN      SOA     ns1.$2.$3. root.$2.$3. (
                     2018033004         ; Serial
                            15M         ; Refresh
                             5M         ; Retry
                           120M         ; Expire
                            600 )       ; Negative Cache TTL
@           IN      NS      ns1
@           IN      MX 10   mail
@           IN      A       $1
ns1         IN      A       $1
$4	        IN      A       $1
mail        IN      A       $1
www         IN      CNAME   $4
webmail	    IN	    CNAME	$4
monitor	    IN      CNAME   $4
info	    IN      CNAME   $4
test        IN      CNAME   $4
nextcloud   IN	    CNAME	$4
EOF

#
# zones/tld.zone
#
echo "Replaceing /etc/bind/zones/tld.zone ..."
cat <<EOF >/etc/bind/zones/$3".zone"
;
; BIND data file for local zone $3;
\$TTL    15M
@       IN      SOA     ns1.$3. root.$3. (
                     2018033005         ; Serial
                            15M         ; Refresh
                             5M         ; Retry
                           120M         ; Expire
                            600 )       ; Negative Cache TTL
@           IN      NS      ns1
@           IN      A       $1
ns1         IN      A       $1
;
;
; Manually added
;
$2 IN NS ns-$2
ns-$2 IN A $1
EOF

#
# /etc/resolv.conf
#
echo "Replaceing /etc/resolv.conf ..."
cat <<EOF >/etc/resolv.conf
domain $2.$3
search $2.$3
nameserver 127.0.0.1
EOF

#
# /etc/hostname
#
echo "Replaceing /etc/hostname ..."
cat <<EOF >/etc/hostname
$4.$2.$3
EOF

#
# /etc/mailname
#
echo "Replaceing /etc/mailname ..."
cat <<EOF >/etc/mailname
mail.$2.$3
EOF

#
# /etc/hosts
#
echo "Replaceing /etc/hosts ..."
cat <<EOF >/etc/hosts
127.0.0.1 localhost
EOF
