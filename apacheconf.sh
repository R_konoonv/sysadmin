#!/bin/bash

#
# Execution example: ./apacheconf.sh teacher est arturerm exm
#

if [ -z "$1" ] ; then
    echo 'Old domin is not specified !!!'
    exit 1
fi

if [ -z "$2" ] ; then
    echo 'Old tld is not specified !!!'
    exit 1
fi

if [ -z "$3" ] ; then
    echo 'New domain is not specified !!!'
    exit 1
fi

if [ -z "$4" ] ; then
    echo 'New tld is not specified !!!'
    exit 1
fi

echo "Backup files to /etc/bak/ ..."
mkdir -p /etc/bak
mkdir -p /etc/bak/apache2
mkdir -p /etc/bak/apache2/conf-enabled 
mkdir -p /etc/bak/apache2/mods-enabled
mkdir -p /etc/bak/apache2/sites-enabled
mkdir -p /etc/bak/apache2/sites-available
cp /etc/apache2/conf-enabled/*.* /etc/bak/apache2/conf-enabled/
cp /etc/apache2/mods-enabled/*.* /etc/bak/apache2/mods-enabled/
cp /etc/apache2/sites-enabled/*.* /etc/bak/apache2/sites-enabled/
cp /etc/apache2/sites-available/*.* /etc/bak/apache2/sites-available/

echo "Removeing old conf and mods ..."
rm /etc/apache2/mods-enabled/*.* 
rm /etc/apache2/conf-enabled/*.*

echo "Updating domain name /etc/apache2/sites-available ..."
find /etc/apache2/sites-available/ -type f -exec sed -i 's/'$1'\.'$2'/'$3'\.'$4'/gI' {} \;
sed -i 's/'$1'\.'$2'/'$3'\.'$4'/gI' /var/www/html/index.html

echo "Updating enabled modules /etc/apache2/mods-enabled ..."
a2enmod -q mpm_prefork
a2enmod -q access_compat
a2enmod -q alias
a2enmod -q authz_core
a2enmod -q authnz_external
a2enmod -q authnz_ldap
a2enmod -q authn_core
a2enmod -q authn_file
a2enmod -q authz_host
a2enmod -q authz_user
a2enmod -q auth_basic
a2enmod -q autoindex
a2enmod -q deflate
a2enmod -q dir
a2enmod -q env
a2enmod -q filter
a2enmod -q http2
a2enmod -q info
a2enmod -q ldap
a2enmod -q mime
a2enmod -q negotiation
a2enmod -q php7.0
a2enmod -q proxy
a2enmod -q proxy_http
a2enmod -q proxy_http2
a2enmod -q reqtimeout
a2enmod -q rewrite
a2enmod -q setenvif
a2enmod -q socache_shmcb
a2enmod -q ssl
a2enmod -q status

echo "Updating enabled cofiguration /etc/apache2/conf-enabled ..."
a2enconf -q serve-cgi-bin
a2enconf -q security
a2enconf -q roundcube
a2enconf -q other-vhosts-access-log
a2enconf -q localized-error-pages
a2enconf -q ldap-certs
a2enconf -q javascript-common
a2enconf -q charset
a2enconf -q phpmyadmin

a2enmod -q cgi

echo "----------Configtest---------"
apache2ctl configtest
echo "------------Restart----------"
systemctl restart apache2
systemctl status apache2